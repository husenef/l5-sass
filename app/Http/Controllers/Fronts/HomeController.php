<?php

namespace App\Http\Controllers\Fronts;

use App\Http\Controllers\Controller;

class HomeController extends Controller{
	
	public function index(){
		return view('front');
	}
}
